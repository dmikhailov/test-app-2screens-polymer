var express = require('express');
var app = express();

// static content
app.use(express.static(__dirname + "/app"));
app.use('/api', express.static(__dirname + '/api'));

// 404
app.get('*', function (req, res) {
    console.log('NOT FOUND: ' + req.url);
    if (req.url.indexOf('/api') == 0) {
        res.status(404);
        res.send({error: 'Not found'});
    } else {
        res.sendFile(__dirname + '/app/index.html');
    }
});

var port = process.env.PORT || 8090;
app.listen(port, function () {
    console.log('server running on port ' + port);
});
